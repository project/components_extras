<?php

namespace Drupal\components_extras;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines an interface for component_theme managers.
 */
interface ComponentThemeManagerInterface extends PluginManagerInterface {
}
